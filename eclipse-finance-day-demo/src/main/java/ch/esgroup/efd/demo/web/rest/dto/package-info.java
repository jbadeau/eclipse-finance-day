/**
 * Data Access Objects used by Spring MVC REST controllers.
 */
package ch.esgroup.efd.demo.web.rest.dto;
