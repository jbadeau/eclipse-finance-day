
/*
 This is the Geb configuration file.
 See: http://www.gebish.org/manual/current/configuration.html
 */
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.ie.InternetExplorerDriver

import ch.esgroup.birdseye.compare.facade.CompareContext
import ch.esgroup.birdseye.compare.facade.DefaultCompareContext
import ch.esgroup.birdseye.compare.managers.FileImageStoreManager

// See: https://code.google.com/p/selenium/wiki/FirefoxDriver
driver = { new FirefoxDriver() }

reportsDir = "target/geb-reports"


birdseye { 
	project = "EclipseFinanceDay"
	baselineVersion = "1.0.0"
	currentVersion  = "1.0.1-SNAPSHOT"
}

environments { 
	baseUrl = "http://localhost:8080/eclipse-finance-day-demo/"
}

