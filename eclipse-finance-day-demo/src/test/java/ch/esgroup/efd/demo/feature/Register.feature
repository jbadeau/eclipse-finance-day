package ch.esgroup.efd.demo.feature

import ch.esgroup.efd.demo.screen.Login
import ch.esgroup.efd.demo.screen.Register

Feature Register

	As a "non-registered user"
	I want to "register"
	In order to "be able to login"
	
	Scenario "create guest user"
		Given I am on the Register screen
		
		when I type "guest" into the Login textfield
		and I type "guest@gmail.com" into the Email textfield
		and I type "guest" into the Password textfield
		and I type "guest" into the PasswordConfirmation textfield
		and I click the Register button
		
		then I am on the Register screen
		and the Success label contains "Please check your email for confirmation."
		
	Scenario "create existing guest user"
		Given I am on the Register screen
		
		when I type "guest" into the Login textfield
		and I type "guest@gmail.com" into the Email textfield
		and I type "guest" into the Password textfield
		and I type "guest" into the PasswordConfirmation textfield
		and I click the Register button
		
		then I am on the Register screen
		and the Fail label contains "Please try again later."

	Scenario "login is required to be at least 1 character"
		Given I am on the Register screen
		
		when I type "guest" into the Login textfield
		and I type "" into the Login textfield
		
		then I am on the Register screen
		and the LoginRequiredError label contains "Your login is required."	

	Scenario "login cannot be longer than 50 characters"
		Given I am on the Register screen
		
		when I type "111111111111111111111111111111111111111111111111111" into the Login textfield
		
		then I am on the Register screen
		and the MaxLengthError label contains "Your login cannot be longer than 50 characters"
