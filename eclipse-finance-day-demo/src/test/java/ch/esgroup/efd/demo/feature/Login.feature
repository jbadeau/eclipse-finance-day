package ch.esgroup.efd.demo.feature

import ch.esgroup.efd.demo.screen.Login
import ch.esgroup.efd.demo.screen.Home

Feature Login

	As a "registered user"
	I want to "login"
	In order to "use the application"

	Scenario "authenticate with admin user"
		Given I am on the Login screen
		
		when I type "admin" into the Login textfield
		and I type "admin" into the Password textfield
		and I click the SignIn button
		
		then I am on the Home screen
		and the Title label contains "Welcome, Java Hipster!"
	
	//@tagScreen true
	Scenario "authenticate with invalid login"
		Given I am on the Login screen
		
		when I type "invalid" into the Login textfield
		and I type "admin" into the Password textfield
		and I click the SignIn button
		
		then I am on the Login screen
		and the Message label contains "Authentication failed! Please check your credentials and try again."
		and the screen looks like LoginAuthenticateWithInvalidLogin excluding Login textfield, Password textfield
		
	Scenario "authenticate with invalid password"
		Given I am on the Login screen
		
		when I type "admin" into the Login textfield
		and I type "invalid" into the Password textfield
		and I click the SignIn button
		
		then I am on the Login screen
		and the Message label contains "Authentication failed! Please check your credentials and try again."

	@param login
	@param password
	@param message
	@dataProvider ch.esgroup.efd.demo.feature.LoginDataProvider		
	Scenario "authenticate with dynamic data"
		Given I am on the Login screen
		
		when I type @login into the Login textfield
		and I type @password into the Password textfield
		and I click the SignIn button
		
		then I am on the Login screen
		and the Message label contains @message
