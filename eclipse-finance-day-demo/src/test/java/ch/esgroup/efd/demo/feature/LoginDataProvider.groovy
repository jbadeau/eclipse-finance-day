package ch.esgroup.efd.demo.feature

import spock.lang.Shared;

class LoginDataProvider {

	def List daten() {
		return [
			[
				"invalid" ,
				"admin",
				"Authentication failed! Please check your credentials and try again."
			],
			[
				"admin" ,
				"invalid",
				"Authentication failed! Please check your credentials and try again."
			]
		]
	}
}
